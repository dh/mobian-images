<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template name="breadcrumb">
        <xsl:param name="list" />
        <xsl:param name="delimiter" select="'/'"  />
        <xsl:param name="reminder" select="$list" />
        <xsl:variable name="newlist">
            <xsl:choose>
                <xsl:when test="contains($list, $delimiter)">
                    <xsl:value-of select="normalize-space($list)" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat(normalize-space($list), $delimiter)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="first" select="substring-before($newlist, $delimiter)" />
        <xsl:variable name="remaining" select="substring-after($newlist, $delimiter)" />
        <xsl:variable name="current" select="substring-before($reminder, $remaining)" />
        <xsl:choose>
            <xsl:when test="$remaining">
                <xsl:choose>
                    <xsl:when test="$first = ''">
                        <a href="/">Home</a>
                    </xsl:when>
                    <xsl:otherwise>
                        <a class="nostyle"> &gt; </a><a href="{$current}"><xsl:value-of select="$first" /></a>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:call-template name="breadcrumb">
                    <xsl:with-param name="list" select="$remaining" />
                    <xsl:with-param name="delimiter" select="$delimiter" />
                    <xsl:with-param name="reminder" select="$reminder" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="$first = ''">
                        <a href="/">Home</a>
                    </xsl:when>
                    <xsl:otherwise>
                        <a class="nostyle"> &gt; </a><a href="{$current}"><xsl:value-of select="$first" /></a>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="filelist">
        <xsl:variable name="name">
            <xsl:value-of select="."/>
        </xsl:variable>
        <xsl:variable name="size">
            <xsl:if test="number(@size) >= 0">
                <xsl:choose>
                    <xsl:when test="round(@size div 1024) &lt; 1"><xsl:value-of select="@size"/></xsl:when>
                    <xsl:when test="round(@size div 1048576) &lt; 1"><xsl:value-of select="format-number((@size div 1024), '0.0')"/>K</xsl:when>
                    <xsl:otherwise><xsl:value-of select="format-number((@size div 1048576), '0.00')"/>M</xsl:otherwise>
                </xsl:choose>
            </xsl:if>
        </xsl:variable>
        <xsl:variable name="date">
            <xsl:value-of select="substring(@mtime,9,2)"/>-<xsl:value-of select="substring(@mtime,6,2)"/>-<xsl:value-of select="substring(@mtime,1,4)"/>
        </xsl:variable>
        <tr>
            <td><a href="{$name}"><xsl:value-of select="."/></a></td>
            <td><xsl:value-of select="$size"/></td>
            <td><xsl:value-of select="$date"/></td>
        </tr>
    </xsl:template>
    <xsl:template match="/">
        <html>
            <head>
                <title>Mobian Images</title>
                <meta charset="utf-8"/>
                <meta name="description" content="Mobian images download"/>
                <meta name="keywords" content="Mobian, Debian, Mobile, mobian project, Phosh, Pinephone, debos, debian based, mobian recipes, prebuilt images"/>
                <meta name="viewport" content="text/html, width=device-width, initial-scale=1, user-scalable=no"/>
                <link rel="stylesheet" href="/.assets/css/images.css" />
                <link rel="shortcut icon" href="/.assets/images/favicon.png" />
            </head>
            <body class="is-preload">
                <div class="crumbs">
                    <xsl:call-template name="breadcrumb">
                        <xsl:with-param name="list" select="$path" />
                    </xsl:call-template>
                </div>
                <div class="header">
                    <h2>Mobian Image Download</h2>
                    Mobian supports a growing range of devices, with installation procedures varying depending on the operating system they were originally meant to run.
                    Please see the relevant device documentation at <a href="https://wiki.mobian.org/doku.php?id=install">the Mobian Wiki</a> for instructions.
                    <br/>
                    Please note that the PinePhone, PinePhone Pro and PineTab require <a href="https://tow-boot.org/getting-started.html">Tow-Boot</a> before they will boot.
                </div>
                <div class="table-wrapper">
                    <table>
                        <thead>
                            <tr>
                                <th><b>Name</b></th>
                                <th><b>Size</b></th>
                                <th><b>Date</b></th>
                            </tr>
                        </thead>
                        <tbody>
                            <xsl:for-each select="list/*[string-length(@size) = 0]">
                                <xsl:sort select="@name" order="ascending"/>
                                <xsl:call-template name="filelist"/>
                            </xsl:for-each>
                            <xsl:for-each select="list/*[string-length(@size) != 0]">
                                <xsl:sort select="@mtime" order="descending"/>
                                <xsl:call-template name="filelist"/>
                            </xsl:for-each>
                        </tbody>
                    </table>
                </div>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
